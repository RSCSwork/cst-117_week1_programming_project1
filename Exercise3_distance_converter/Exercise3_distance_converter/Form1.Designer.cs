﻿namespace Exercise3_distance_converter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numberToConvertTxtBox = new System.Windows.Forms.TextBox();
            this.convertFeetToYardsBtn = new System.Windows.Forms.Button();
            this.numberConvertedTxtBox = new System.Windows.Forms.TextBox();
            this.EnterDistanceLbl = new System.Windows.Forms.Label();
            this.DistanceConvertedLbl = new System.Windows.Forms.Label();
            this.convertFeetToMetersBtn = new System.Windows.Forms.Button();
            this.convertYardsToMetersBtn = new System.Windows.Forms.Button();
            this.clearBtn = new System.Windows.Forms.Button();
            this.headerLbl = new System.Windows.Forms.Label();
            this.messageLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // numberToConvertTxtBox
            // 
            this.numberToConvertTxtBox.Location = new System.Drawing.Point(393, 79);
            this.numberToConvertTxtBox.MinimumSize = new System.Drawing.Size(200, 50);
            this.numberToConvertTxtBox.Name = "numberToConvertTxtBox";
            this.numberToConvertTxtBox.Size = new System.Drawing.Size(200, 26);
            this.numberToConvertTxtBox.TabIndex = 0;
            // 
            // convertFeetToYardsBtn
            // 
            this.convertFeetToYardsBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.convertFeetToYardsBtn.Location = new System.Drawing.Point(92, 252);
            this.convertFeetToYardsBtn.MinimumSize = new System.Drawing.Size(500, 50);
            this.convertFeetToYardsBtn.Name = "convertFeetToYardsBtn";
            this.convertFeetToYardsBtn.Size = new System.Drawing.Size(501, 61);
            this.convertFeetToYardsBtn.TabIndex = 1;
            this.convertFeetToYardsBtn.Text = "Convert Feet to Yards";
            this.convertFeetToYardsBtn.UseVisualStyleBackColor = true;
            this.convertFeetToYardsBtn.Click += new System.EventHandler(this.Button1_Click);
            // 
            // numberConvertedTxtBox
            // 
            this.numberConvertedTxtBox.Location = new System.Drawing.Point(393, 148);
            this.numberConvertedTxtBox.MinimumSize = new System.Drawing.Size(200, 50);
            this.numberConvertedTxtBox.Name = "numberConvertedTxtBox";
            this.numberConvertedTxtBox.Size = new System.Drawing.Size(200, 26);
            this.numberConvertedTxtBox.TabIndex = 2;
            // 
            // EnterDistanceLbl
            // 
            this.EnterDistanceLbl.AutoSize = true;
            this.EnterDistanceLbl.Location = new System.Drawing.Point(89, 79);
            this.EnterDistanceLbl.MinimumSize = new System.Drawing.Size(200, 50);
            this.EnterDistanceLbl.Name = "EnterDistanceLbl";
            this.EnterDistanceLbl.Size = new System.Drawing.Size(243, 50);
            this.EnterDistanceLbl.TabIndex = 3;
            this.EnterDistanceLbl.Text = "Please enter a value for distance:";
            // 
            // DistanceConvertedLbl
            // 
            this.DistanceConvertedLbl.AutoSize = true;
            this.DistanceConvertedLbl.Location = new System.Drawing.Point(89, 148);
            this.DistanceConvertedLbl.MinimumSize = new System.Drawing.Size(200, 50);
            this.DistanceConvertedLbl.Name = "DistanceConvertedLbl";
            this.DistanceConvertedLbl.Size = new System.Drawing.Size(200, 50);
            this.DistanceConvertedLbl.TabIndex = 4;
            this.DistanceConvertedLbl.Text = "The distance converted:";
            // 
            // convertFeetToMetersBtn
            // 
            this.convertFeetToMetersBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.convertFeetToMetersBtn.Location = new System.Drawing.Point(93, 319);
            this.convertFeetToMetersBtn.MinimumSize = new System.Drawing.Size(500, 50);
            this.convertFeetToMetersBtn.Name = "convertFeetToMetersBtn";
            this.convertFeetToMetersBtn.Size = new System.Drawing.Size(501, 61);
            this.convertFeetToMetersBtn.TabIndex = 5;
            this.convertFeetToMetersBtn.Text = "Convert Feet to Meters";
            this.convertFeetToMetersBtn.UseVisualStyleBackColor = true;
            this.convertFeetToMetersBtn.Click += new System.EventHandler(this.Button2_Click);
            // 
            // convertYardsToMetersBtn
            // 
            this.convertYardsToMetersBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.convertYardsToMetersBtn.Location = new System.Drawing.Point(93, 386);
            this.convertYardsToMetersBtn.MinimumSize = new System.Drawing.Size(500, 50);
            this.convertYardsToMetersBtn.Name = "convertYardsToMetersBtn";
            this.convertYardsToMetersBtn.Size = new System.Drawing.Size(501, 61);
            this.convertYardsToMetersBtn.TabIndex = 6;
            this.convertYardsToMetersBtn.Text = "Convert Yards to Meters";
            this.convertYardsToMetersBtn.UseVisualStyleBackColor = true;
            this.convertYardsToMetersBtn.Click += new System.EventHandler(this.Button3_Click);
            // 
            // clearBtn
            // 
            this.clearBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clearBtn.Location = new System.Drawing.Point(93, 453);
            this.clearBtn.MinimumSize = new System.Drawing.Size(500, 50);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(501, 61);
            this.clearBtn.TabIndex = 7;
            this.clearBtn.Text = "Clear";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.Button4_Click);
            // 
            // headerLbl
            // 
            this.headerLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.headerLbl.AutoSize = true;
            this.headerLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headerLbl.Location = new System.Drawing.Point(93, 25);
            this.headerLbl.MinimumSize = new System.Drawing.Size(500, 25);
            this.headerLbl.Name = "headerLbl";
            this.headerLbl.Size = new System.Drawing.Size(500, 29);
            this.headerLbl.TabIndex = 8;
            this.headerLbl.Text = "Welcome to the Distance Convertor!";
            this.headerLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // messageLbl
            // 
            this.messageLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.messageLbl.AutoSize = true;
            this.messageLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageLbl.Location = new System.Drawing.Point(94, 214);
            this.messageLbl.MinimumSize = new System.Drawing.Size(500, 25);
            this.messageLbl.Name = "messageLbl";
            this.messageLbl.Size = new System.Drawing.Size(500, 25);
            this.messageLbl.TabIndex = 9;
            this.messageLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 526);
            this.Controls.Add(this.messageLbl);
            this.Controls.Add(this.headerLbl);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.convertYardsToMetersBtn);
            this.Controls.Add(this.convertFeetToMetersBtn);
            this.Controls.Add(this.DistanceConvertedLbl);
            this.Controls.Add(this.EnterDistanceLbl);
            this.Controls.Add(this.numberConvertedTxtBox);
            this.Controls.Add(this.convertFeetToYardsBtn);
            this.Controls.Add(this.numberToConvertTxtBox);
            this.Name = "Form1";
            this.Text = "Distance Convertor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox numberToConvertTxtBox;
        private System.Windows.Forms.Button convertFeetToYardsBtn;
        private System.Windows.Forms.TextBox numberConvertedTxtBox;
        private System.Windows.Forms.Label EnterDistanceLbl;
        private System.Windows.Forms.Label DistanceConvertedLbl;
        private System.Windows.Forms.Button convertFeetToMetersBtn;
        private System.Windows.Forms.Button convertYardsToMetersBtn;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.Label headerLbl;
        private System.Windows.Forms.Label messageLbl;
    }
}

