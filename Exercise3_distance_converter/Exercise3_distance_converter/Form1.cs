﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise3_distance_converter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try {
                messageLbl.Text = "";
                double feetInputBtn1, yardsOutputBtn1; //Declaring two variables, one for the input of feet for button 1, and the other which will become the ouput.
                feetInputBtn1 = double.Parse(numberToConvertTxtBox.Text);
                yardsOutputBtn1 = feetInputBtn1 / 3; //feet divided by 3 in order to get the yards, answer can be floating
                String yardsStringBtn1 = String.Format("{0:0.000}", yardsOutputBtn1); //formatting for 3 decimal places.  
                numberConvertedTxtBox.Text = Convert.ToString(yardsStringBtn1);
            } catch{
                messageLbl.Text = "Input was not a value, please try again!";
            }//ends try catch block
        }//ends button 1 click

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                messageLbl.Text = "";
                double feetInputBtn2, metersOutputBtn2; //declaring two variables, one for input of feet for button 2, and the other for meters which will be the output of button 2
                feetInputBtn2 = double.Parse(numberToConvertTxtBox.Text);
                metersOutputBtn2 = feetInputBtn2 * 0.3048; //feet multiplied by .3048 to get meters, checked online to ensure conversion.
                String metersStringBtn2 = String.Format("{0:0.000}", metersOutputBtn2); //formatting for 3 decimal places.  
                numberConvertedTxtBox.Text = Convert.ToString(metersStringBtn2);
            } catch{
                messageLbl.Text = "Input was not a value, please try again!";
            }//ends try catch block
        }//ends button 2 click

        private void Button3_Click(object sender, EventArgs e)
        {
            try
            {
                messageLbl.Text = "";
                double yardsInputBtn3, metersOutputBtn3; //declaring two variables, one for input of yards for button 3, and the other for meters which will be the output of button 3.
                yardsInputBtn3 = double.Parse(numberToConvertTxtBox.Text);
                metersOutputBtn3 = yardsInputBtn3 * 0.9144; //yards multiplied by .9144 to get meters, checked online to ensure conversion.
                String metersStringBtn3 = String.Format("{0:0.000}", metersOutputBtn3); //formatting for 3 decimal places.  
                numberConvertedTxtBox.Text = Convert.ToString(metersStringBtn3);
            } catch{
                messageLbl.Text = "Input was not a value, please try again!";
            }//ends try catch block
        }//ends button 3 click

        private void Button4_Click(object sender, EventArgs e)
        {
            numberToConvertTxtBox.Text = "";
            numberConvertedTxtBox.Text = "";
            messageLbl.Text = "";
        }//ends button 4 click
    }//ends Form 1 class
}//ends namespace
